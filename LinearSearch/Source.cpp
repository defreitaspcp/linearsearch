/*
Author: de Freitas, P.C.P
Description - Linear Search
*/
#include<iostream>
#include<vector>
#include<cstdio>
using namespace std;
int linearSearch(const vector<int> & v, int element)
{
	for (size_t i = 0; i < v.size(); i++)
	{
		if (v[i] == element)
		{
			printf("Element[%i]=%i\n",i,v[i]);
			return 1;
		}
	}
	return -1;
}
int main()
{
	vector<int> nums{ 1, 3, 5, 7 };
	linearSearch(nums,0);
	linearSearch(nums, 2);
	linearSearch(nums, 5);
}