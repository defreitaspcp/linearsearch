Busca Linear
--
Busca linear para expressar um tipo de pesquisa em vetores ou listas de modo sequencial, i. e., elemento por elemento, de modo que a função do tempo em relação ao número de elementos é linear, ou seja, cresce proporcionalmente. Num vetor ordenado, essa não é a pesquisa mais eficiente.[1]

Referências
--
[1]https://pt.wikipedia.org/wiki/Busca_linear